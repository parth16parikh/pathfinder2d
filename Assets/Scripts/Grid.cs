﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    public bool m_DrawGridGizmos;
    public LayerMask m_ObstacleMask;
    public Vector2 m_GridWorldSize;
    public float m_NodeRadius;

    private Node[,] m_Grid;
    float m_NodeDiameter;
    int m_GridSizeX, m_GridSizeY;

    public int MaxHeapSize
    {
        get
        {
            return m_GridSizeX * m_GridSizeY;
        }
    }

    void Awake()
    {
        m_NodeDiameter = m_NodeRadius * 2.0f;
        m_GridSizeX = Mathf.RoundToInt(m_GridWorldSize.x / m_NodeDiameter);
        m_GridSizeY = Mathf.RoundToInt(m_GridWorldSize.y / m_NodeDiameter);

        CreateGrid();
    }

    public Node NodeFromWoldPoint(Vector3 worldPosition)
    {
        float l_PercentX = (worldPosition.x + (m_GridWorldSize.x / 2.0f)) / m_GridWorldSize.x;
        float l_PercentY = (worldPosition.y + (m_GridWorldSize.y / 2.0f)) / m_GridWorldSize.y;
        l_PercentX = Mathf.Clamp01(l_PercentX);
        l_PercentY = Mathf.Clamp01(l_PercentY);

        int x = Mathf.RoundToInt((m_GridSizeX - 1) * l_PercentX);
        int y = Mathf.RoundToInt((m_GridSizeY - 1) * l_PercentY);

        return m_Grid[x, y];
    }

    private void CreateGrid()
    {
        m_Grid = new Node[m_GridSizeX, m_GridSizeY];
        Vector3 l_worldBottomLeft = transform.position - new Vector3(m_GridWorldSize.x / 2.0f, m_GridWorldSize.y / 2.0f, 0.0f);
        for (int x = 0; x < m_GridSizeX; x++)
        {
            for (int y = 0; y < m_GridSizeY; y++)
            {
                Vector3 l_WorldPosition = l_worldBottomLeft + Vector3.right * (x * m_NodeDiameter + m_NodeRadius) + Vector3.up * (y * m_NodeDiameter + m_NodeRadius);
                NodeType l_NodeType = NodeType.Walkable;
                if (Physics2D.OverlapCircle(l_WorldPosition, m_NodeRadius, m_ObstacleMask))
                {
                    l_NodeType = NodeType.Obstacle;
                }
                m_Grid[x, y] = new Node(l_WorldPosition, l_NodeType, x, y);
            }
        }

        SetupNeighbours();
    }

    private void SetupNeighbours()
    {
        for (int x = 0; x < m_GridSizeX; x++)
        {
            for (int y = 0; y < m_GridSizeY; y++)
            {
                m_Grid[x, y].Neighbours = GetNeighbours(m_Grid[x, y]);
            }
        }
    }

    public List<Node> GetNeighbours(Node node)
    {
        List<Node> l_Neighbours = new List<Node>();

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                {
                    continue;
                }

                int neighbourXIndex = node.GridX + x;
                int neighbourYIndex = node.GridY + y;

                if (neighbourXIndex >= 0 && neighbourXIndex < m_GridSizeX && neighbourYIndex >= 0 && neighbourYIndex < m_GridSizeY)
                {
                    l_Neighbours.Add(m_Grid[neighbourXIndex, neighbourYIndex]);
                }
            }
        }

        return l_Neighbours;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, m_GridWorldSize);

        if (m_Grid != null && m_DrawGridGizmos)
        {
            foreach (Node n in m_Grid)
            {
                Gizmos.color = n.NodeType == NodeType.Obstacle ? Color.red : Color.white;
                Gizmos.DrawCube(n.WorldPosition, Vector3.one * (m_NodeDiameter - 0.1f));
            }
        }


    }
}
