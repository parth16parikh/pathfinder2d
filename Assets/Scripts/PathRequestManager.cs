﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PathRequestManager : MonoBehaviour
{
    Queue<PathRequst> m_PathRequests = new Queue<PathRequst>();
    PathRequst m_CurrentPathRequest;

    static PathRequestManager m_Instance;
    bool m_ProcessingPath;
    PathFinder m_PathFinder;

    private void Awake()
    {
        m_Instance = this;
        m_PathFinder = GetComponent<PathFinder>();
    }

    public static void RequestPath(Vector3 startPath, Vector3 endPath, Action<List<Node>, bool > callback)
    {
        PathRequst l_PathRequest = new PathRequst(startPath, endPath, callback);
        m_Instance.m_PathRequests.Enqueue(l_PathRequest);
        m_Instance.TryProcessNextPathRequest();
    }

    private void TryProcessNextPathRequest()
    {
        if (m_ProcessingPath)
        {
            return;
        }

        if(m_PathRequests.Count <= 0)
        {
            return;
        }

        m_CurrentPathRequest = m_PathRequests.Dequeue();
        m_ProcessingPath = true;
        m_PathFinder.StartPathFinding(m_CurrentPathRequest.startPath, m_CurrentPathRequest.endPath, FinishedPathFinding);
    }

    private void FinishedPathFinding(List<Node> wayPoints, bool success)
    {
        m_CurrentPathRequest.callback(wayPoints, success);
        m_ProcessingPath = false;
        TryProcessNextPathRequest();
    }

    struct PathRequst
    {
        public Vector3 startPath;
        public Vector3 endPath;
        public Action<List<Node>, bool> callback;

        public PathRequst(Vector3 _startPath, Vector3 _endPath, Action<List<Node>, bool> _callback)
        {
            startPath = _startPath;
            endPath = _endPath;
            callback = _callback;
        }
    }
}
