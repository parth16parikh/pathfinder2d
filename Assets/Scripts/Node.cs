﻿using UnityEngine;
using System.Collections.Generic;
public enum NodeType
{
    Walkable,
    Obstacle
}

public class Node : IHeapItem<Node>
{
    public NodeType NodeType { get; private set; }
    public Vector3 WorldPosition { get; private set; }

    public int GridX { get; private set; }
    public int GridY { get; private set; }

    public int DistanceFromStartNode { get; set; }
    public int DistanceFromTargetNode { get; set; }
    public Node Parent { get; set; }

    public int FinalDistance { get => (DistanceFromStartNode + DistanceFromTargetNode); }
    int heapIndex;
    public int HeapIndex { get => heapIndex; set => heapIndex = value; }
    public List<Node> Neighbours { get; set; }

    public Node(Vector3 worldPosition, NodeType nodeType, int gridX, int gridY)
    {
        NodeType = nodeType;
        WorldPosition = worldPosition;
        GridX = gridX;
        GridY = gridY;
    }

    public int CompareTo(Node compareNode)
    {
        int compare = FinalDistance.CompareTo(compareNode.FinalDistance);
        if(compare == 0)
        {
            compare = DistanceFromTargetNode.CompareTo(compareNode.DistanceFromTargetNode);
        }
        return -compare;
    }
}
