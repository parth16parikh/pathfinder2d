﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    [SerializeField]
    private Transform m_Target;
    [SerializeField]
    private float m_NearestDistanceToReach = 0.1f;
    [SerializeField]
    private bool m_DrawGizmos;

    float speed = 20.0f;
    private Coroutine m_Coroutine;
    private List<Node> m_CurrentWayPoints;
    private int m_WayPointIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        PathRequestManager.RequestPath(transform.position, m_Target.position, OnPathFound);
    }

    private void OnPathFound(List<Node> wayPoints, bool success)
    {
        if (success)
        {
            if(m_Coroutine != null)
            {
                StopCoroutine(m_Coroutine);
            }
            m_CurrentWayPoints = wayPoints;
            m_Coroutine = StartCoroutine(FollowPath());
        }
    }

    IEnumerator FollowPath()
    {
        Node l_CurrentWayPoint = m_CurrentWayPoints[m_WayPointIndex];

        while (true)
        {
            if (Vector2.Distance(transform.position, l_CurrentWayPoint.WorldPosition) < m_NearestDistanceToReach)
            {
                m_WayPointIndex++;

                if(m_WayPointIndex >= m_CurrentWayPoints.Count)
                {
                   yield break;
                }

                l_CurrentWayPoint = m_CurrentWayPoints[m_WayPointIndex];
            }

            transform.position = Vector3.MoveTowards(transform.position, l_CurrentWayPoint.WorldPosition, speed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

    private void OnDrawGizmos()
    {
        if (m_DrawGizmos && m_CurrentWayPoints != null)
        {
            for(int i = m_WayPointIndex; i < m_CurrentWayPoints.Count; i++)
            {
                Gizmos.color = Color.black;
                Gizmos.DrawCube(m_CurrentWayPoints[i].WorldPosition, Vector3.one);

                if(i == m_WayPointIndex)
                {
                    Gizmos.DrawLine(transform.position, m_CurrentWayPoints[i].WorldPosition);
                }
                else
                {
                    Gizmos.DrawLine(m_CurrentWayPoints[i - 1].WorldPosition, m_CurrentWayPoints[i].WorldPosition);
                }
            }
        }        
    }
}
