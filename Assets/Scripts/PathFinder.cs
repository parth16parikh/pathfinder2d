﻿using System.Diagnostics;
using System.Collections.Generic;
using System;
using System.Collections;
using UnityEngine;

public class PathFinder : MonoBehaviour
{
    public Transform m_Seeker;
    public Transform m_Target;

    Grid m_Grid;
    PathRequestManager m_PathRequestManager;

    private void Awake()
    {
        m_Grid = GetComponent<Grid>();
        m_PathRequestManager = GetComponent<PathRequestManager>();
    }

    public void StartPathFinding(Vector3 startPos, Vector3 endPos, Action<List<Node>, bool> pathFoundCallback)
    {
        FindPath(startPos, endPos, pathFoundCallback);
    }

    void FindPath(Vector3 startPosition, Vector3 targetPosition, Action<List<Node>, bool> pathFoundCallback)
    {
        Stopwatch sw = new Stopwatch();
        sw.Start();

        List<Node> waypoints = new List<Node>();
        bool pathSuccess = false;

        Node l_StartNode = m_Grid.NodeFromWoldPoint(startPosition);
        Node l_TargetNode = m_Grid.NodeFromWoldPoint(targetPosition);

        if(l_StartNode.NodeType == NodeType.Walkable || l_TargetNode.NodeType == NodeType.Walkable)
        {
            Heap<Node> l_OpenNodes = new Heap<Node>(m_Grid.MaxHeapSize);
            HashSet<Node> l_CloseNodes = new HashSet<Node>();

            l_OpenNodes.Add(l_StartNode);

            while (l_OpenNodes.Count > 0)
            {
                Node l_CurrentNode = l_OpenNodes.RemoveFirst();

                l_CloseNodes.Add(l_CurrentNode);

                if (l_CurrentNode == l_TargetNode)
                {
                    sw.Stop();
                    pathSuccess = true;
                    print("Path found : " + sw.ElapsedMilliseconds + " ms");
                    break;
                }

                List<Node> l_Neighbours = l_CurrentNode.Neighbours;
                for (int i = 0; i < l_Neighbours.Count; i++)
                {
                    if (l_Neighbours[i].NodeType != NodeType.Walkable || l_CloseNodes.Contains(l_Neighbours[i]))
                    {
                        continue;
                    }
                    int newDistanceToNeighbour = l_CurrentNode.DistanceFromStartNode + GetDistance(l_CurrentNode, l_Neighbours[i]);

                    if (newDistanceToNeighbour < l_Neighbours[i].DistanceFromStartNode || !l_OpenNodes.Contains(l_Neighbours[i]))
                    {
                        l_Neighbours[i].DistanceFromStartNode = newDistanceToNeighbour;
                        l_Neighbours[i].DistanceFromTargetNode = GetDistance(l_Neighbours[i], l_TargetNode);
                        l_Neighbours[i].Parent = l_CurrentNode;

                        if (!l_OpenNodes.Contains(l_Neighbours[i]))
                        {
                            l_OpenNodes.Add(l_Neighbours[i]);
                        }
                    }
                }
            }
        }

        if (pathSuccess)
        {
            waypoints = RetracePath(l_StartNode, l_TargetNode);
        }
        pathFoundCallback(waypoints, pathSuccess);
    }

    List<Node> RetracePath(Node startNode, Node endNode)
    {
        List<Node> l_Path = new List<Node>();
        Node currentNode = endNode;

        while (currentNode != startNode)
        {
            l_Path.Add(currentNode);
            currentNode = currentNode.Parent;
        }
        l_Path = SimplifyPath(l_Path);
        l_Path.Reverse();
        return l_Path;
    }

    List<Node> SimplifyPath(List<Node> wayPoints)
    {
        List<Node> l_WayPoints = new List<Node>();
        Vector2 directionOld = Vector2.zero;
        
        for(int i = 1; i < wayPoints.Count; i++)
        {
            Vector2 directionNew = new Vector2(wayPoints[i - 1].GridX - wayPoints[i].GridX, wayPoints[i - 1].GridY - wayPoints[i].GridY);
            if (directionOld != directionNew)
            {
                l_WayPoints.Add(wayPoints[i]);
                directionOld = directionNew;
            }
        }

        return l_WayPoints;
    }

    int GetDistance(Node nodeA, Node nodeB)
    {
        int distanceX = Mathf.Abs(nodeA.GridX - nodeB.GridX);
        int distanceY = Mathf.Abs(nodeA.GridY - nodeB.GridY);

        if (distanceX < distanceY)
        {
            return 14 * distanceX + (10 * (distanceY - distanceX));
        }

        return 14 * distanceY + (10 * (distanceX - distanceY));
    }
}
