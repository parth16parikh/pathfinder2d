﻿using System;
using System.Collections.Generic;

public class Heap<T> where T : IHeapItem<T>
{
    T[] m_Items;
    int m_CurrentItemCount;

    public Heap(int maxHeapSize)
    {
        m_Items = new T[maxHeapSize];
    }

    public void Add(T item)
    {
        item.HeapIndex = m_CurrentItemCount;
        m_Items[m_CurrentItemCount] = item;
        SortUp(item);
        m_CurrentItemCount++;
    }

    public T RemoveFirst()
    {
        T l_FirstItem = m_Items[0];
        m_CurrentItemCount--;
        m_Items[0] = m_Items[m_CurrentItemCount];
        m_Items[0].HeapIndex = 0;
        SortDown(m_Items[0]);
        return l_FirstItem;
    }

    public void UpdateItem(T item)
    {
        SortUp(item);
    }

    public int Count
    {
        get
        {
            return m_CurrentItemCount;
        }
    }

    public bool Contains(T item)
    {
        return Equals(m_Items[item.HeapIndex], item);
    }

    void SortDown(T item)
    {
        while (true)
        {
            int l_ChildIndexLeft = (item.HeapIndex * 2) + 1;
            int l_ChildIndexRight = (item.HeapIndex * 2) + 2;
            int l_SwapIndex = 0;

            if (l_ChildIndexLeft < m_CurrentItemCount)
            {
                l_SwapIndex = l_ChildIndexLeft;
                if (l_ChildIndexRight < m_CurrentItemCount)
                {
                    if (m_Items[l_ChildIndexLeft].CompareTo(m_Items[l_ChildIndexRight]) < 0)
                    {
                        l_SwapIndex = l_ChildIndexRight;
                    }
                }

                if (item.CompareTo(m_Items[l_SwapIndex]) < 0)
                {
                    Swap(item, m_Items[l_SwapIndex]);
                }
                else
                {
                    return;
                }

            }
            else
            {
                return;
            }
        }
    }



    void SortUp(T item)
    {
        int l_ParentIndex = -1;

        while (true)
        {
            l_ParentIndex = (item.HeapIndex - 1) / 2;
            T l_ParentItem = m_Items[l_ParentIndex];

            if (item.CompareTo(l_ParentItem) > 0)
            {
                Swap(item, l_ParentItem);
            }
            else
            {
                break;
            }
        }
    }

    void Swap(T itemA, T itemB)
    {
        m_Items[itemA.HeapIndex] = itemB;
        m_Items[itemB.HeapIndex] = itemA;
        int l_ItemBIndex = itemB.HeapIndex;
        itemB.HeapIndex = itemA.HeapIndex;
        itemA.HeapIndex = l_ItemBIndex;
    }
}

public interface IHeapItem<T> : IComparable<T>
{
    int HeapIndex
    {
        get;
        set;
    }
}